const aws = require('aws-sdk')
aws.config.update({region: 'ap-southeast-2'})

/**
 * Sends an email.
 *
 * @param {string[]} addressees A list of email addressees.
 * @param {string} subject The email subject.
 * @param {string} text The email text.
 * @param {string} html The email HTML.
 * @return {string} The message ID of the email, if successful.
 */
const sendEmail = (addressees, subject, text, html) => {
  const emailParams = {
    Destination: {
      // CcAddresses: [],
      ToAddresses: addressees
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: html
        },
        Text: {
          Charset: 'UTF-8',
          Data: text
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: subject
      }
    },
    Source: 'travis@travisvalenti.com'
  }

  return new aws.SES().sendEmail(emailParams).promise()
    .then(data => {
      return data.MessageId
    })
}

const handler = () => {
  sendEmail(['kpaiy42@gmail.com'], 'SES Test', 'Hello from JS.', '<html>Hello from JS.</html>')
    .then(id => console.log(`Message ID: ${id}`))
}

exports.handler = handler
